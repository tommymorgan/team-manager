import Inferno from "inferno";
import { Router } from "inferno-router";
import createBrowserHistory from "history/createBrowserHistory";
import routes from "../universal/routes";

const browserHistory = createBrowserHistory();

Inferno.render(
  <Router history={browserHistory}>{routes}</Router>,
  document.getElementById("app")
);
