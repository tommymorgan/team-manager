import Inferno, { linkEvent } from "inferno";
import Component from 'inferno-component';

const handleInput = (props, event) => {
	props.validateValue(event.target.value);
};

const handleSubmit = (props, event) => {
};

export default class PlayerForm extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		this.props.componentWillMount();
	}

	render() {
		const props = this.props;

		return (
			<form onSubmit={handleSubmit}>
				<div>
					<input
						onInput={linkEvent(props, handleInput)}
						placeholder={"First name"}
						value={this.props.firstName}
					/>
				</div>
				<div>
					<input
						onInput={linkEvent(props, handleInput)}
						placeholder={"Last name"}
						value={this.props.lastName}
						/>
				</div>
				<div>
					<input
						onInput={linkEvent(props, handleInput)}
						placeholder={"Email"}
						type="email"
						/>
				</div>
				<div>
					<input
						onInput={linkEvent(props, handleInput)}
						placeholder={"Phone number"}
						type="tel"
					/>
				</div>
				<div>
					<label>
						<input
							onChange={linkEvent(props, handleInput)}
							type="checkbox"
						/>
						Supports SMS
					</label>
				</div>
			</form>
		);
	}
}
