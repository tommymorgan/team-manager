import Inferno from "inferno";
import { Link } from "inferno-router";
import Component from 'inferno-component';

const renderPlayer = (player) => {
	return (
		<div>
			<Link to={`/player/${player.id}`}>{"Player"}</Link>
		</div>
	);
};

export default class PlayerList extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		this.props.componentWillMount();
	}

	render() {
		return (
			<div>
				{this.props.players.map(renderPlayer)}
			</div>
		);
	}
}
