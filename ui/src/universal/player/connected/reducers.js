import {GET_PLAYER, GET_PLAYER_LIST} from "./actions";

export default (state = {}, action) => {
	switch (action.type) {
	case GET_PLAYER:
		state = Object.assign({}, state, {
			player: action.player
		});
		break;
	case GET_PLAYER_LIST:
		state = Object.assign({}, state, {
			list: action.players
		});
		break;
	}

	return state;
};
