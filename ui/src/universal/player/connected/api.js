import ApolloClient from 'apollo-client-preset';
import { HttpLink } from 'apollo-link-http';
import fetch from 'node-fetch';
import gql from 'graphql-tag';

const link = new HttpLink({ uri: 'http://localhost:3000/graphql', fetch });
const client = new ApolloClient({
	link
});

const getPlayer = (id) => {
	return client.query({
		query: gql`
			query Player {
				player(id: "${id}") {
					id
					first_name
					last_name
					phone {
						number
						supports_sms
					}
					email
				}
			}
		`,
	});
};

const getPlayerList = () => {
	return client.query({
		query: gql`
			query Players {
				players {
					id
					first_name
					last_name
					phone {
						number
						supports_sms
					}
					email
				}
			}
		`,
	});
};

export default {
	getPlayer,
	getPlayerList
};
