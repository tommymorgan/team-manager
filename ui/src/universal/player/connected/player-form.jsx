import PlayerForm from "../player-form";
import {connect} from "inferno-redux";
import {getPlayer} from "./actions";
import api from "./api";

const mapStateToProps = ({players}) => {
	const player = players.player || {};

	return {
		firstName: player.first_name,
		lastName: player.last_name,
		email: player.email,
		phone: player.phone
	};
};

const mapDispatchToProps = (dispatch, ownProps) => {
	return {
		componentWillMount: () => {
			getPlayer(ownProps.params.id)(api.getPlayer)(dispatch);
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayerForm);
