import List from "../list";
import {connect} from "inferno-redux";
import {getPlayerList} from "./actions";
import api from "./api";
import PlayerActions from "./actions";

const mapStateToProps = ({players}) => {
	return {
		players: players.list || []
	};
};

const mapDispatchToProps = (dispatch) => {
	return {
		componentWillMount: () => {
			getPlayerList()(api.getPlayerList)(dispatch);
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(List);
