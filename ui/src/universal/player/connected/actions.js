import ReduxThunk from "redux-thunk";

export const GET_PLAYER = "GET_PLAYER";
export const GET_PLAYER_LIST = "GET_PLAYER_LIST";

export const getPlayer = (id) => (api) => (dispatch) => {
	api(id).then((result) => dispatch({
		type: GET_PLAYER,
		player: result.data.player
	})).catch(error => {
		console.error(error);
	});
};

export const getPlayerList = () => (api) => (dispatch) => {
	api().then((result) => dispatch({
		type: GET_PLAYER_LIST,
		players: result.data.players
	})).catch(error => {
		console.error(error);
	});
};
