import Inferno from "inferno";
import { Link } from "inferno-router";

export default ({children}) => {
	return (
		<div class="team-manager-app">
			{
				children
				||
				<Link to="/players">{"Players"}</Link>
			}
		</div>
	);
};
