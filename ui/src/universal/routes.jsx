import { Route, IndexRoute } from "inferno-router";
import { Provider } from "inferno-redux";
import { combineReducers, createStore } from "redux";
import App from "./app";
import PlayerForm from "./player/connected/player-form";
import PlayerList from "./player/connected/list";
import players from "./player/connected/reducers";

const store = createStore(combineReducers({
	players
}));

const routes = (
	<Provider store={store}>
		<Route path="/" component={App}>
			<Route path="/players" component={PlayerList} />
			<Route path="/player/:id" component={PlayerForm} />
		</Route>
	</Provider>
);

export default routes;
