import mongoose from "mongoose";
import Promise from "es6-promise";

mongoose.connect("mongodb://localhost/team-manager", {
	promiseLibrary: Promise,
	useMongoClient: true
});

let Schema = mongoose.Schema;

let PlayerSchema = new Schema({
	first_name: String,
	last_name: String,
	email: [{
		type: String,
		validate: {
			validator: (v) => /@.+\./.test(v),
			message: "{VALUE} is not a valid email address"
		}
	}],
	phone: [{
		number: String,
		supports_sms: Boolean
	}],
	created_date: {
		type: Date,
		default: Date.now
	}
});

export const Players = mongoose.model("Players", PlayerSchema);
