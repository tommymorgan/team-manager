import {
	makeExecutableSchema,
	addMockFunctionsToSchema,
} from 'graphql-tools';
import mocks from './mocks'
import resolvers from './resolvers';

const typeDefs = `
type Phone {
	number: String!
	supports_sms: Boolean
}

type Player {
	id: String
	first_name: String
	last_name: String
	email: [String]
	phone: [Phone]
}

type Query {
	players: [Player]
	player(id: String!): Player
	testString: String
}
`;

const schema = makeExecutableSchema({ typeDefs, resolvers });

// addMockFunctionsToSchema({ schema, mocks });

export default schema;