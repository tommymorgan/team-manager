import casual from 'casual';

const mocks = {
	String: () => 'It works!',
	Query: () => ({
		player: (root, args) => {
			return {
				first_name: args.first_name,
				last_name: args.last_name
			};
		}
	}),
	Player: () => ({
		first_name: () => casual.first_name,
		last_name: () => casual.last_name
	})
};

export default mocks;