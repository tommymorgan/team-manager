import {Players} from "./collections";

const resolvers = {
	Query: {
		players(root, args) {
			return Players.find({}).then((player) => player);
		},
		player(root, args) {
			return Players.findOne().then((player) => player);
		}
	},
};

export default resolvers;