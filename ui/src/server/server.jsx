import express from "express";
import { renderToString } from "inferno-server";
import { match, RouterContext, StaticRouter } from "inferno-router";
import routes from "../universal/routes";
import bodyParser from "body-parser";
import { graphqlExpress, graphiqlExpress } from "apollo-server-express";
import graphQLSchema from "../data/schema";

let app = express();
let port = process.env.PORT || 3000;

app.use(express.static("dist", {
	extensions: ["js"],
	index: false,
	setHeaders: function (res, path, stat) {
		res.set("x-timestamp", Date.now())
	}
}));

app.use("/graphql", bodyParser.json(), graphqlExpress({ schema: graphQLSchema }));
app.get("/graphiql", graphiqlExpress({ endpointURL: "/graphql" })); // if you want GraphiQL enabled

// Exactly like in React! 
function Html({children}) {
	return (
		<html lang="en">
			<head>
				<title>Team Manager</title>
				<meta name="viewport" content="width=device-width, initial-scale=1"/>
				<meta charSet="utf-8"/>
				<meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
			</head>
			<body>
				<div id="app" data-what="server">{children}</div>
				<script src="/client.bundle.js"></script>
			</body>
		</html>
	);
}

// This is fired every time the server side receives a request
app.use(handleRender);

function handleRender(req, res) {
	const renderProps = match(routes, req.originalUrl);

	if (renderProps.redirect) {
		return res.redirect(renderProps.redirect);
	}
	
	if (renderProps) {
		// You can also check renderProps.components or renderProps.routes for
		// your "not found" component or route respectively, and send a 404 as
		// below, if you"re using a catch-all route.
		// console.log(`RENDER_PROPS: ${JSON.stringify(renderProps)}`);
		const content = (<Html><RouterContext {...renderProps}/></Html>);
		// console.log(`CONTENT: ${renderToString(content)}`);
		
		// Send the rendered page back to the client
		res.status(200).send(renderToString(content));
	} else {
		res.status(404).send("Not found");
	}
}

app.listen(port, function(err) {
	if (err) {
		console.log(err);
	}
});	