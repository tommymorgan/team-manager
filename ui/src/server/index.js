import express from "express";
import Inferno from "inferno";
import { renderToString } from "inferno-server";
import { match, RouterContext } from "inferno-router";
import routes from "../universal/routes";

let app = express();
let port = process.env.PORT || 3000;

app.use(express.static("dist"));

// This is fired every time the server side receives a request
app.use(handleRender);

function handleRender(req, res) {
	const renderProps = match(routes, req.originalUrl);

	if (renderProps.redirect) {
		return res.redirect(renderProps.redirect);
	}
	
	if (renderProps) {
		// You can also check renderProps.components or renderProps.routes for
		// your "not found" component or route respectively, and send a 404 as
		// below, if you"re using a catch-all route.
		const html = renderToString(<RouterContext {...renderProps} />);

		// Send the rendered page back to the client
		res.status(200).send(renderFullPage(html));	 
	} else {
		res.status(404).send("Not found");
	}
}

// Exactly like in React! 
function renderFullPage(html) {
	return `
		<!DOCTYPE html>
		<html lang="en">
			<head>
				<title>Team Manager</title>
					<meta name="viewport" content="width=device-width, initial-scale=1">
					<meta charset="utf-8">
					<meta http-equiv="X-UA-Compatible" content="IE=edge">
			</head>
			<body>
				<div id="app">${html}</div>
				<script src="/index.bundle.js"></script>
			</body>
		</html>
	`;
}

app.listen(port, function(err) {
	if (err) {
		console.log(err);
	}
});