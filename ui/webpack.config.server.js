const baseConfig = require("./webpack.config.universal");
const nodeExternals = require("webpack-node-externals");

module.exports = Object.assign({}, baseConfig, {
	devtool: "inline-source-map",
	entry: {
		server: "./src/server/server.jsx"
	},
	externals: [nodeExternals()],
	target: "node"
});
