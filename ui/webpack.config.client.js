const baseConfig = require("./webpack.config.universal");

module.exports = Object.assign({}, baseConfig, {
	devtool: "source-map",
	entry: {
		client: "./src/client/index.js"
	},
	target: "web"
});
