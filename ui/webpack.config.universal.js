const HtmlWebpackPlugin = require("html-webpack-plugin");
const path = require("path");
const webpack = require("webpack");

module.exports = {
	devServer: {
		contentBase: "./dist",
		hot: true
	},
	module: {
		rules: [{
			test: /\.css$/,
			use: ["style-loader", "css-loader"]
		}, {
			test: /\.(js|jsx)$/,
			include: path.resolve(__dirname, "src"),
			loader: "babel-loader"
		}]
	},
	output: {
		chunkFilename: "[name].bundle.js",
		filename: "[name].bundle.js",
		path: path.resolve(__dirname, "dist")
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: "Team Manager",
			template: "index.ejs"
		}),
		new webpack.HotModuleReplacementPlugin()
	],
	resolve: {
		extensions: ['.js', '.jsx']
	}
};
