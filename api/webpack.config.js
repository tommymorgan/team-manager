const nodeExternals = require("webpack-node-externals");
const path = require("path");
const webpack = require("webpack");

module.exports = {
	devtool: "source-map",
	entry: {
		api: "./src/server.js"
	},
	externals: [nodeExternals()],
	module: {
		rules: [{
			test: /\.(js)$/,
			include: path.resolve(__dirname, "src"),
			loader: "babel-loader"
		}]
	},
	output: {
		chunkFilename: "[name].bundle.js",
		filename: "[name].bundle.js",
		path: path.resolve(__dirname, "dist")
	},
	resolve: {
		extensions: ['.js']
	}
};
