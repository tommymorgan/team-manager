import express from "express";
import mongoose from "mongoose";
import Player from "./api/models/players";
import bodyParser from "body-parser";

let app = express();
let port = process.env.PORT || 3001;

mongoose.connect("mongodb://localhost/team-manager");
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

import routes from "./api/routes/players";
routes(app);

app.listen(port);