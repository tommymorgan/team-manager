import {
	createPlayer,
	deletePlayer,
	getPlayer,
	listPlayers,
	updatePlayer
} from "../controllers/players";

export default (app) => {
	app.route("/api/players")
		.get(listPlayers)
		.post(createPlayer);

	app.route("/api/players/:id")
		.get(getPlayer)
		.put(updatePlayer)
		.delete(deletePlayer);
};
