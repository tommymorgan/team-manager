import mongoose from "mongoose";

const Player = mongoose.model("Players");

export const createPlayer = (req, res) => {
	let newPlayer = Player.create(req.body);

	newPlayer.save((err, player) => {
		if (err) {
			res.send(err);
		}

		res.json(player);
	});
};

export const deletePlayer = (req, res) => {
	Player.remove({
		_id: req.params.id
	}, (err, player) => {
		if (err) {
			res.send(err);
		}
		
		res.json({
			message: "Player successfully deleted"
		});
	});
};

export const getPlayer = (req, res) => {
	Player.findById(req.params.id, (err, player) => {
		if (err) {
			res.send(err);
		}

		res.json(player);
	});
};

export const listPlayers = (req, res) => {
	Player.find({}, (err, players) => {
		if (err) {
			res.send(err);
		}

		res.json(players);
	})
};

export const updatePlayer = (req, res) => {
	Player.findByIdAndUpdate(req.params.id, req.body, {
		new: true
	}, (err, player) => {
		if (err) {
			res.send(err);
		}

		res.json(player);
	});
};