import mongoose from "mongoose";

let Schema = mongoose.Schema;

let PlayerSchema = new Schema({
	first_name: String,
	last_name: String,
	email: [{
		type: String,
		validate: {
			validator: (v) => /@.+\./.test(v),
			message: "{VALUE} is not a valid email address"
		}
	}],
	phone: [{
		number: String,
		supports_sms: Boolean
	}],
	created_date: {
		type: Date,
		default: Date.now
	}
});

export default mongoose.model("Players", PlayerSchema);